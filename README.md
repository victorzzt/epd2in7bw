# epd2in7bw

#### This is a fork to epd2in7b for 2.7 inch
* There's already a forked version: epd2in7b : https://github.com/Bodmer/EPD_Libraries/tree/master/epd2in7b
    * However due to its for the B varient (not confirmed), the screen flicker several times before it settles for bw version
* The above library is a fork of epd7x5
    * Original link: https://github.com/williwasser/epd7x5
* This fork will have the same dependency as the original library
    * sudo apt-get install libgd2-dev # libgd
    * For the library itself, since it's only for very specific purpose, I won't publish it on npm as no point of doing it
        * npm install --save https://bitbucket.org/victorzzt/epd2in7bw.git seems to work very well currently
    * so will need to install directly from the repository
  
* This Library removed button support as it's not present
* Fastlut seems make it flicker less, but red still can't be used
    * Copied lut from python version
* Did not remove the fastlut option, sloppy, but just add it in, it will ignore it.